# Multi-level models for the analysis of COVID-19 seroprevalence studies

These R scripts generate the models and figures found in our paper [A note on COVID-19 seroprevalence studies: a meta-analysis using hierarchical modelling](https://www.medrxiv.org/content/10.1101/2020.05.03.20089201v1). The script `mcmc-script.R` runs the Bayesian model using Stan, while `glmm-script.R` runs the GLMM model using lme4.

You will need the following R packages: `tidyverse`, `rstan`, `lme4`, `merTools`, `scales`, and `shinystan` (optional).
