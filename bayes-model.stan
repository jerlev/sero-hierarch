data {
  int<lower=1> N_tests;    // number of test brands
  int<lower=1> size_fpos[N_tests];  // number of negative controls for each test brand
  int<lower=0> x_fpos[N_tests]; // number of false positives in neg. controls 
  int<lower=1> size_fneg[N_tests]; // number of positive controls for each test brand
  int<lower=0> x_fneg[N_tests]; // number of false negative in pos. controls
  int<lower=1> N_studies;     // number of studies
  int<lower=1> test_id_study[N_studies]; // test brand used in each study
  int<lower=1> size_study[N_studies]; // size of each study
  int<lower=0> x_study[N_studies]; // number of positive observations in each study
}


parameters {
  vector[N_tests] alpha_fpos; // logit(P(test = yes | disease = no, test brand))
  vector[N_tests] alpha_fneg; // logit(P(test = no | disease = yes, test brand))
  vector[N_studies] alpha_prev; // logit(P(disease = yes | region))
  // normal hierarchical priors in logit space
  real mu_alpha_fpos;
  real<lower=0> sigma_alpha_fpos;
  real mu_alpha_fneg;
  real<lower=0> sigma_alpha_fneg;
  real mu_alpha_prev;
  real<lower=0> sigma_alpha_prev;
}


transformed parameters {
  vector<lower=0, upper=1>[N_tests] p_fpos; // false positive rate
  vector<lower=0, upper=1>[N_tests] p_fneg; // false negative rate
  vector<lower=0, upper=1>[N_studies] p_prev; // antibody preavlence
  p_fpos = inv_logit(alpha_fpos);
  p_fneg = inv_logit(alpha_fneg);
  p_prev = inv_logit(alpha_prev);
}


model {
  // The whole inference is encoded in loops instead of using
  // vectorized statements because of the dependence on test brands
  for (i in 1:N_tests) {
    x_fpos[i] ~ binomial_logit(size_fpos[i], alpha_fpos[i]);
  }

  for (i in 1:N_tests) {
    x_fneg[i] ~ binomial_logit(size_fneg[i], alpha_fneg[i]);
  }

  for (i in 1:N_studies) {
    x_study[i] ~ binomial(size_study[i], p_prev[i] * (1 - p_fneg[test_id_study[i]]) + (1 - p_prev[i]) * p_fpos[test_id_study[i]]);
  }

  // Hierarchical priors
  alpha_fpos ~ normal(mu_alpha_fpos, sigma_alpha_fpos);
  mu_alpha_fpos ~ normal(0, 10);
  sigma_alpha_fpos ~ exponential(1);

  alpha_fneg ~ normal(mu_alpha_fneg, sigma_alpha_fneg);
  mu_alpha_fneg ~ normal(0, 10);
  sigma_alpha_fneg ~ exponential(1);

  alpha_prev ~ normal(mu_alpha_prev, sigma_alpha_prev);
  mu_alpha_prev ~ normal(0, 10);
  sigma_alpha_prev ~ exponential(1);
}
